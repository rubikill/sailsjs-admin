/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */
var bcrypt = require('bcrypt');


module.exports.bootstrap = function(cb) {

  var defaultAdminEmail = 'admin@example.com';
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  User.findOne({ email: defaultAdminEmail }).exec(function(err, user){
    if (err) throw new Error(err);
    if (user) cb();
    if (!user) {
      User.create({ email: defaultAdminEmail, password: '12341234' }, function(err, user){
        if (err) throw new Error(err);
        console.log('Auto-generating admin@example.com user with password 12341234');
        cb();
      });
    }

  })

};
