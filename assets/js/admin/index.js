function ngaConfig(NgAdminConfigurationProvider, RestangularProvider, ApiSchema) {
		console.log(arguments);
		RestangularProvider
			.addFullRequestInterceptor(function(element, operation, what, url, headers, params, httpConfig) {
				if (operation == 'getList' /*&& what == 'user'*/) {
					params.skip = (params._page - 1) * params._perPage;
					params.limit = params._perPage;
					delete params._page;
					delete params._perPage;
					delete params._sortDir;
					delete params._sortField;
				}
				return { params: params };
			});

		/*
			Configure ngAdmin
		*/
		var nga = NgAdminConfigurationProvider;
		// set the main API endpoint for this admin
		//TODO: use '<%- apiUrl %>' to configure API baseUrl
		var app = nga.application('SailsJS Admin UI').baseApiUrl('/');

		// define an entity mapped by the http://localhost:3000/posts endpoint

		var entities = ApiSchema.entities;

		var ngaEntitiesMap = {};
		var eFields = [];

		var addEntity = function(eName, i, arr){
			ngaEntitiesMap[eName] = nga.entity(eName);
			var entity = entities[eName];

			Object.keys(entity).forEach(function(aName){
				var attr = entity[aName];
				var field = nga.field(aName, attr.type).label(attr.label);
				if (attr.type === 'choice'){
					field.choices(attr.choices);
				}
				if (attr.type === 'referenced_list'){
					if (ngaEntitiesMap[attr.$ref] !== undefined) {
						field.targetEntity(ngaEntitiesMap[attr[attr.type]]) // Select a target Entity
						.targetField(nga.field(ngaEntitiesMap[attr[attr.type]].name())) // Select a label Field
					} else {
						//Really dirty exit condition --> refactor
						var idx = arr.indexOf(attr.$ref);
						arr.splice(idx, 1);
						addEntity(attr.$ref, i, arr);
					}
				}

				eFields.push(field);
			});

			app.addEntity(ngaEntitiesMap[eName]);


			ngaEntitiesMap[eName].listView().fields(eFields).listActions(['show', 'edit', 'delete']);
			ngaEntitiesMap[eName].creationView().fields(eFields);
			ngaEntitiesMap[eName].editionView().fields(eFields);
			ngaEntitiesMap[eName].showView().fields(eFields);
		};


		//Configure ng-admin with entities
		Object.keys(entities).forEach(addEntity);

		// set the list of fields to map in each post view
		// post.dashboardView().fields();
		// post.listView().fields();
		// post.creationView().fields();
		// post.editionView().fields();

		nga.configure(app);
};


Schema().load({}).then(function(res){

	angular.module('admin', [
		'ng-admin',
		'admin.login'
	])
	.config([
		'NgAdminConfigurationProvider',
		'RestangularProvider',
		function(NgAdminConfigurationProvider, RestangularProvider){
			ngaConfig(NgAdminConfigurationProvider, RestangularProvider, JSON.parse(res))
		}
	]);

	angular.element(document).ready(function(){
    //At this point API config is loaded, Angular is loaded and ng-admin is loaded
    angular.bootstrap(document, ['admin']);
  });

})

;
