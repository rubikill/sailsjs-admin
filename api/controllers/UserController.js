/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	attributes: {
		email: {
			label: 'E-mail',
			type: 'email'
		},
		password: {
			label: 'Password',
			type: 'password'
		}
	}
};
